//
//  STSAppDelegate.m
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSAppDelegate.h"

#import "STSPinsViewController.h"
#import "STSTruckViewController.h"
#import "STSContactsViewController.h"
#import "STSCardViewController.h"


#import "UINavigationBar+SetAppearence.h"
#import "UITabBarController+CustomAppearence.h"

@implementation STSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.viewControllers = @[
                    [self navigationFrom:[self pinsController]],
                    [self navigationFrom:[self truckController]],
                    [self navigationFrom:[self cardController]],
                    [self navigationFrom:[self contactController]]
                    ];
    
    
    [self.tabBarController customizeTabView];
    [UINavigationBar customizeNavigationBar];
    
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
    return YES;
}

-(UIViewController *)pinsController
{
    return [[STSPinsViewController alloc] initWithNibName:@"STSPinsViewController" bundle:nil];
}
-(UIViewController *)truckController
{
    return [[STSTruckViewController alloc] initWithNibName:@"STSTruckViewController" bundle:nil];
}
-(UIViewController *)contactController
{
    return [[STSContactsViewController alloc] initWithNibName:@"STSContactsViewController" bundle:nil];
}
-(UIViewController *)cardController
{
    return [[STSCardViewController alloc] initWithNibName:@"STSCardViewController" bundle:nil];
}
-(UINavigationController *)navigationFrom:(UIViewController *)root_view
{
    UINavigationController *pins = [[UINavigationController alloc] initWithRootViewController:root_view];
    return pins;
    
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

@end
