//
//  STS_SImpleStationFactory.h
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "STSStationViewsFactory.h"

@interface STSSimpleStationFactory : NSObject<STSStationViewsFactory>


// redifene it in child object
-(NSString *)simplePinName;
-(NSString *)calloutPinName;
-(NSString *)simplePinImageName;

@end
