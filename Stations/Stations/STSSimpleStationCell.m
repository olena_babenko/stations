//
//  STSSimpleStationCell.m
//  Stations
//
//  Created by Olena on 9/21/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSSimpleStationCell.h"

@implementation STSSimpleStationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setData:(STS_Station *)data
{
    _data = data;
    _top_title.text = data.title;
    _bottom_title.text = data.subtitle;
}
@end
