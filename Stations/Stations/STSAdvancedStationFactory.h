//
//  STSAdvancedStationFactory.h
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "STSStationViewsFactory.h"
#import "STSSimpleStationFactory.h"

@interface STSAdvancedStationFactory : STSSimpleStationFactory

@end
