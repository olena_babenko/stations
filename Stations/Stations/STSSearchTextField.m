//
//  STSTextField.m
//  Stations
//
//  Created by Olena on 9/18/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSSearchTextField.h"

@implementation STSSearchTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)awakeAfterUsingCoder:(NSCoder *)aDecoder
{
    [self addBorderStyle];
    [self addBackground];
    
    [self makeSimilarFont];
    [self addSearchIcon];
    
    return self;
}

// we should set an image as a background, so no borders

-(void)addBorderStyle
{
    self.borderStyle = UITextBorderStyleNone;
}


-(void)addBackground
{
    NSString *imageNamed = @"searchfield";
    UIImage *currentImage = [UIImage imageNamed:imageNamed];
    if (currentImage) {
        UIImage *ButtonBackground = [currentImage
                                     resizableImageWithCapInsets:UIEdgeInsetsMake(15, 19, 15, 19)];
        
        [self setBackground:ButtonBackground];
    }
}

//custom font


-(void)makeSimilarFont
{
    
    self.font = [UIFont systemFontOfSize:12];
    
}

-(void)addSearchIcon
{
    [self addPadding];
    UIImage *icon_image = [UIImage imageNamed:@"icon_top_search"];
    [self addIcon:icon_image];
}

-(void)addPadding
{
    
    CGRect padding_frame  =[self IconPaddingFrame];;
    UIView *paddingView = [[UIView alloc] initWithFrame:padding_frame];
    if (self.leftView) {
        [self.leftView removeFromSuperview];
    }
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}
-(void)addIcon:(UIImage *) icon
{
    if (icon) {
        
        UIImageView *image_view_icon = [[UIImageView alloc] initWithFrame:[self iconFrame]];
        image_view_icon.image = icon;
        image_view_icon.contentMode = UIViewContentModeScaleAspectFit;
        
        [self addSubview:image_view_icon];
    }
    
}

-(CGFloat)iconSide
{
    return self.frame.size.height;
}
-(CGFloat)offset_icon_x
{
    return 5;
    
}
-(CGFloat)offset_icon_y
{
    return 7;
    
}
-(CGRect)iconFrame
{
    CGFloat x = [self offset_icon_x];
    CGFloat y = [self offset_icon_y];
    CGFloat w = [self iconSide] - x*2;
    CGFloat h = [self iconSide] - y*2;
    return  CGRectMake(x, y, w, h);
}

-(CGRect)IconPaddingFrame
{
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat w = [self iconSide];
    CGFloat h = w;
    return  CGRectMake(x, y, w, h);
}


@end
