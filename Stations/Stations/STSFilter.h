//
//  STSFilter.h
//  Stations
//
//  Created by Olena on 9/21/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STSFilter : NSObject
{
    NSMutableArray *result;
}
@property (strong, nonatomic, readonly) NSArray *unfiltered_data;

-(id)initWithArray:(NSArray *) source;

-(void)reset;
-(void)filterZip:(id) value;
-(void)filterCity:(id) value;

-(NSArray *)getResult;
@end
