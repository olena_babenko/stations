//
//  STSAdvancedStationCell.m
//  Stations
//
//  Created by Olena on 9/21/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSAdvancedStationCell.h"

@implementation STSAdvancedStationCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
