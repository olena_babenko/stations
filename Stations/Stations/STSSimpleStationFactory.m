//
//  STS_SImpleStationFactory.m
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSSimpleStationFactory.h"

#import "STSCalloutStationView.h"
#import "STSSimpleStationCell.h"

@implementation STSSimpleStationFactory

// this name to reuse pins on map

-(NSString *)simplePinName
{
    return @"SimpleStationAnnotation";
}

-(NSString *)calloutPinName
{
    return @"CalloutStationView";
}
-(NSString *)simplePinImageName
{
    return @"pin_truck.png";
}
-(MKAnnotationView *)getPinViewInMap:(MKMapView *)map withAnnotation:(id<MKAnnotation>) annotation
{
    NSString *name =[self simplePinName];
    MKAnnotationView *annotationView =[map dequeueReusableAnnotationViewWithIdentifier:name];
    if (!annotationView) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                       reuseIdentifier:name];
        annotationView.canShowCallout = NO;
        UIImage *pin_image = [UIImage imageNamed:[self simplePinImageName]];
        annotationView.image = pin_image;
        
        UIImageView *image_view = [self shadowImageViewWithY:pin_image.size.height andX:pin_image.size.width];
        [annotationView addSubview:image_view];
        

    }
    return annotationView;
    
}
-(UIImageView *)shadowImageViewWithY:(CGFloat)Y andX:(CGFloat)X
{
    UIImageView *image_view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pin_shadow"]];
    CGFloat startX =X/2 +image_view.frame.size.width/2;
    CGFloat startY = Y- image_view.frame.size.height/2;
    image_view.center = CGPointMake(startX, startY);
    return image_view;
}

-(MKAnnotationView *)getCalloutViewInMap:(MKMapView *)map withAnnotation:(id<MKAnnotation>) annotation andDelegate:(id<STSCalloutDelegate>)delegate
{
    NSString *name =[self calloutPinName];
    STSCalloutStationView *annotationView = (STSCalloutStationView *)[map dequeueReusableAnnotationViewWithIdentifier:name];
    if (!annotationView) {
        annotationView = [[STSCalloutStationView alloc] initWithAnnotation:annotation reuseIdentifier:name];
        
        
    }else
        annotationView.annotation = annotation;
    
    
    if (annotationView.delegate) {
        annotationView.delegate = nil;
    }
    annotationView.delegate = delegate;
    
    
    return annotationView;
    
    
}

-(UITableViewCell *)getCellInTable:(UITableView *)table withData:(id) cell_data
{
    NSString *reusable_name = @"STSSimpleStationCell";
    
    STSSimpleStationCell *cell = [table dequeueReusableCellWithIdentifier:reusable_name];
    
    if(!cell)
    {
        UINib *cell_loader = [UINib nibWithNibName:@"STSSimpleStationCell" bundle:nil];
        NSArray *topLevelItems = [cell_loader instantiateWithOwner:table options:nil];
        cell = topLevelItems[0];
        
    }
    cell.data = cell_data;
    
    return cell;
    
}

-(CGFloat)cellHeight
{
    return 50;
}

@end
