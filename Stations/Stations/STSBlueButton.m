//
//  STSButton.m
//  Stations
//
//  Created by Olena on 9/18/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSBlueButton.h"

@implementation STSBlueButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(id)awakeAfterUsingCoder:(NSCoder *)aDecoder
{
    NSString *active = @"top_button_blue_active";
    NSString *normal = @"top_button_blue";
    [self setBackgroundImage:[self buttonImageName:normal] forState:UIControlStateNormal];
    [self setBackgroundImage:[self buttonImageName:active] forState:UIControlStateHighlighted];
    return self;
}

-(UIImage *)buttonImageName:(NSString *) name
{
    UIImage *currentImage = [UIImage imageNamed:name];
    if (currentImage) {
        UIImage *ButtonBackground = [currentImage
                                     resizableImageWithCapInsets:UIEdgeInsetsMake(15, 8, 15, 8)];
        
        return ButtonBackground;
    }
    return nil;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
