//
//  STSTruckViewController.m
//  Stations
//
//  Created by Olena on 9/18/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSTruckViewController.h"

@interface STSTruckViewController ()

@end

@implementation STSTruckViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tab_icon_truck_active"]withFinishedUnselectedImage:[UIImage imageNamed:@"tab_icon_truck"]];
        self.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
