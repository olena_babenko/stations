//
//  STSSimpleStationCell.h
//  Stations
//
//  Created by Olena on 9/21/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STS_Station.h"

@interface STSSimpleStationCell : UITableViewCell



@property (weak, nonatomic) IBOutlet UILabel *top_title;
@property (weak, nonatomic) IBOutlet UILabel *bottom_title;
@property (weak, nonatomic) IBOutlet UIImageView *img;

@property (weak, nonatomic) STS_Station *data;

@end
