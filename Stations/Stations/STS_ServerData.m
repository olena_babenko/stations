//
//  STS_ServerData.m
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STS_ServerData.h"

@implementation STS_ServerData

-(id)initWithParser:(id<STSParser>)parser
{
    STS_ServerData*new_data =[super init];
    if (new_data) {
        new_data.parser = parser;
    }

    return new_data;

}
-(NSURL *)requestURL
{
    NSString *absoluteURL = @"https://erhverv.unox.dk/poi/ds2.csv";
    NSURL *url = [NSURL URLWithString:absoluteURL];
    return url;
}
-(void)getAllPinsSuccess:(void (^)(NSArray *))complete orFail:(void (^)(NSError *))fail
{
    
    
    _complete = complete;
    _fail = fail;
    
    //load gata
    
    NSURL *request_url = [self requestURL];
    
    [self startRequestWithURL:request_url];
    
    
    
    
    
}

// Start request and requestURL to redefine it in every Server Data child object

-(void)startRequestWithURL:(NSURL *)request
{
    NSString *fileString = [NSString stringWithContentsOfURL:request encoding:NSASCIIStringEncoding error:nil];
    
    NSArray *result = [_parser parse:fileString];
    if (result) {
        if (result.count>0) {
            if (_complete) {
                _complete(result);
                _complete = nil;
                return;
            }
        }
    }

    if(_fail)
    {
        _fail([self requestFailError]);
        _fail = nil;
    }
}
// it can be any other errors or class to generate different errors
-(NSError *)requestFailError
{
    
}

@end
