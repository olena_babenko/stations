//
//  UINavigationBar+SetAppearence.h
//  Stations
//
//  Created by Olena on 9/18/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (SetAppearence)


+(void)customizeNavigationBar;
@end
