//
//  STSParserCSV.m
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSParserCSV.h"

@implementation STSParserCSV

-(NSArray*)parse:(id)data
{
    if ([data isKindOfClass:[NSString class]]) {
        
        NSMutableArray *result = [[NSMutableArray alloc] init];
        
        NSString *fileString  = (NSString *)data;
        
        //in this example using only \r charecter but it can be any newline or separation characters
        
        NSArray *contentArray = [fileString componentsSeparatedByString:@"\r"];
        for (NSString *item in contentArray) {

            STS_Station *new_station = [[STS_Station alloc] initWithString:item];
            [result addObject:new_station];
        }
        return result;
    }
    return nil;
}
@end
