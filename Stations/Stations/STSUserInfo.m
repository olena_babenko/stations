//
//  STSUserInfo.m
//  Stations
//
//  Created by iOS Developer on 20.09.13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSUserInfo.h"

@implementation STSUserInfo

static STSUserInfo *sharedInstance = nil;

// Its a singleton to get user info

+ (STSUserInfo *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}
+ (id)allocWithZone:(NSZone*)zone {
    return [self sharedInstance];
}
- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        
        //Init and Create database Objects
        
    }
    return self;
}

-(void)updateLocation
{
    if (!locationManager) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    if (!locationManager.delegate) {
        locationManager.delegate = self;
    }
    
    
    [locationManager startUpdatingLocation];
}
-(void)stopUpdating
{
    [locationManager stopUpdatingLocation];
    
#if TARGET_IPHONE_SIMULATOR
    
    self.current_location = [[CLLocation alloc] initWithLatitude:55.905886 longitude:12.475332];
    return;
#endif
    
    self.current_location = last_location;
}
-(BOOL)shouldStopAtPoint:(CLLocation *)new_location
{
    if (last_location) {
        if (last_location.coordinate.latitude == new_location.coordinate.latitude) {
            if (last_location.coordinate.longitude == new_location.coordinate.longitude) {
                return YES;
            }
        }
    }
    return NO;
}
#pragma mark - Map location info

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if ([self shouldStopAtPoint:[locations lastObject]]) {
        [self stopUpdating];
    }else
    {
        last_location = [locations lastObject];
    }
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"location Manager Error %@", [error description]);
}

#pragma mark - get Region Near Current Location
-( MKCoordinateRegion)locationDefaultRegion
{
    // region scale
    float region_half_side = 0.2;
    
    MKCoordinateSpan span;
    span.latitudeDelta = region_half_side;
    span.longitudeDelta = region_half_side;
    
    MKCoordinateRegion region;
    region.center = _current_location.coordinate;
    region.span = span;
    
    return region;
    
}


@end
