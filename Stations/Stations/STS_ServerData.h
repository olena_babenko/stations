//
//  STS_ServerData.h
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "STSParser.h"


//can  be a parrent object to  load data from server
@interface STS_ServerData : NSObject


@property (strong,nonatomic) id<STSParser> parser;

@property(copy,nonatomic) void(^complete)(NSArray *components);
@property(copy,nonatomic) void(^fail)(NSError* fail_reason);

-(void)getAllPinsSuccess:(void(^)(NSArray *components))complete orFail:(void(^)(NSError *fail_reason))fail;

-(id)initWithParser:(id<STSParser>)parser;
@end
