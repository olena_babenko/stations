//
//  STSCalloutDelegate.h
//  Stations
//
//  Created by iOS Developer on 20.09.13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol STSCalloutDelegate <NSObject>

-(void)stsCalloutSelected:(id<MKAnnotation>) annotation;

@end
