//
//  STSPinsViewController.h
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STS_ServerData.h"
#import "STSStationViewsFactory.h"
#import "STSCalloutDelegate.h"
#import "STSFilter.h"
#import "STSSearchTextField.h"
#import "STSBarButtonItem.h"

@interface STSPinsViewController : UIViewController<MKMapViewDelegate, STSCalloutDelegate, UITextFieldDelegate, UITableViewDataSource,
    UITableViewDelegate
>
{
    STS_ServerData *data_loader;
    id<MKAnnotation> selected_annotation;
    
    STSFilter *data_filter;
    NSArray *filtered_data;
    
    STSBarButtonItem *top_btn;
    
}

// class helper to build pins callouts view and table cells
@property (strong, nonatomic) id<STSStationViewsFactory> simple_factory;
@property (strong, nonatomic) id<STSStationViewsFactory> advanced_factory;

@property (weak, nonatomic) IBOutlet MKMapView *map_view;
@property (weak, nonatomic) IBOutlet UITableView *station_list;


@property (weak, nonatomic) IBOutlet STSSearchTextField *search_txt;


- (IBAction)to_current_location:(id)sender;
- (IBAction)to_filterZip:(id)sender;


@end
