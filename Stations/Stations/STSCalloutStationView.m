//
//  STSCalloutStationView.m
//  Stations
//
//  Created by Olena on 9/18/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSCalloutStationView.h"


@implementation STSCalloutStationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    if (selected) {
        if (_delegate) {
            
            [_delegate stsCalloutSelected:self.annotation];
        }
        
    }
    [super setSelected:selected animated:animated];
}
-(id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if(self)
    {
        
        //customize VIEW!
        self.backgroundColor = [UIColor clearColor];
        self.canShowCallout = NO;
        
        // lift it up for 68 points
        // and move it x to be centeres near triangle 
        self.centerOffset = CGPointMake(68, -65);
        
        
        self.frame = CGRectMake(0, 0, 190, 65);
        sub_view = [[[NSBundle mainBundle] loadNibNamed:@"STSCalloutStationTopView" owner:self options:nil] objectAtIndex:0];
        sub_view.data = annotation;
        [self addSubview: sub_view];
        
        
    }
    return self;
}
-(void)setAnnotation:(id<MKAnnotation>)annotation
{
    [super setAnnotation:annotation];
    sub_view.data = annotation;
}

@end
