//
//  UITabBarController+CustomAppearence.m
//  Stations
//
//  Created by Olena on 9/18/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "UITabBarController+CustomAppearence.h"

@implementation UITabBarController (CustomAppearence)


-(void)customizeTabView
{
    [[self tabBar] setBackgroundImage:[UIImage imageNamed:@"tab_bg"]];
    [[self tabBar] setSelectionIndicatorImage:[UIImage imageNamed:@"tab_bg_active"]];
    //[[self.tabBarController tabBar] setTintColor:[UIColor blackColor]];
}
@end
