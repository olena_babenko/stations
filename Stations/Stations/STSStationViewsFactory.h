//
//  STSStationViewsFactory.h
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "STSCalloutDelegate.h"

@protocol STSStationViewsFactory <NSObject>

-(MKAnnotationView *)getPinViewInMap:(MKMapView *)map withAnnotation:(id<MKAnnotation>) annotation;


-(MKAnnotationView *)getCalloutViewInMap:(MKMapView *)map withAnnotation:(id<MKAnnotation>) annotation andDelegate:(id<STSCalloutDelegate>) delegate;


-(UITableViewCell *)getCellInTable:(UITableView *)table withData:(id) cell_data;

-(CGFloat)cellHeight;
@end
