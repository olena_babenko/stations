//
//  STSFilter.m
//  Stations
//
//  Created by Olena on 9/21/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSFilter.h"

@implementation STSFilter

// init data we have to filter
-(id)initWithArray:(NSArray *) source
{
    
    self = [super init];
    
    if (self) {
    
        self.unfiltered_data = source;
        
        result = [[NSMutableArray alloc] initWithArray:source];
    }
    
    return self;
    
}
-(void)reset
{
    if (result) {
        [result removeAllObjects];
    }
    result = [self.unfiltered_data mutableCopy];
}
-(void)setUnfiltered_data:(NSArray *)unfiltered_data
{
    _unfiltered_data = unfiltered_data;
}

-(void)filterZip:(id) value
{

    if (result.count >0) {
        
        result = [self filterProperty:@"zip" withPart:value];
    }


}
-(void)filterCity:(id) value
{
    if (result.count >0) {
        
        result = [self filterProperty:@"city" withPart:value];
    }
}

-(NSArray *)getResult
{
    return result;
}


//filter data functions

// for filters that need Equal property data

-(NSMutableArray *)filterProperty:(NSString *)property_name withValue:(id)value
{
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"%K == %@",property_name, value];
    NSArray *filtered_objects = [result filteredArrayUsingPredicate:filter];
    return ([filtered_objects mutableCopy]);
}

// for filters that needs object that contains property
-(NSMutableArray *)filterProperty:(NSString *)property_name withPart:(id)part_value
{
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", property_name, part_value];
    NSArray *filtered_objects = [result filteredArrayUsingPredicate:filter];
    
    return ([filtered_objects mutableCopy]);
}


@end
