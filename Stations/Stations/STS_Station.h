//
//  STS_Station.h
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    no_icon = 0,
    ic_mail = 1,
    ic_password = 2,
    ic_search = 3,
    
} LeftSideIcon;

@interface STS_Station : NSObject<MKAnnotation>
{
    CLLocationDegrees _latitude;
	CLLocationDegrees _longitude;
    
    NSString *string_for_copy;
    
}

@property (strong, nonatomic) NSString *zip;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *type;

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (assign, nonatomic) BOOL is_advanced;
@property (assign, nonatomic) BOOL is_diesel;

@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;


@property (assign,nonatomic) BOOL is_copy;

-(id)initWithString:(NSString *)data_row;

@end
