//
//  STSBarButtonItem.m
//  Stations
//
//  Created by Olena on 9/21/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSBarButtonItem.h"

@implementation STSBarButtonItem

-(id)initWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action
{
    STSBarButtonItem * _self = [super initWithImage:image style:style target:target action:action];
    if (_self) {
        
        //init
        [_self customizeBackGround];
        _self.showing_map = YES;
        
    }
    return _self;
    
}
-(void)customizeBackGround
{
    UIImage *barBackImage = [UIImage imageNamed:@"top_button_blue"];
    [[UIBarButtonItem appearance] setBackgroundImage:barBackImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    UIImage *barBackImage_active = [UIImage imageNamed:@"top_button_blue_active"];
    [[UIBarButtonItem appearance] setBackgroundImage:barBackImage_active forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    
}
-(void)setShowing_map:(BOOL)showing_map
{
    _showing_map = showing_map;
    
    NSString *image_name;
    if (_showing_map) {
        image_name = @"icon_top_list";
    }else
    {
        image_name = @"icon_top_map";
    }
    UIImage *barButtonImage = [UIImage imageNamed:image_name];
    [self setImage:barButtonImage];
}

@end
