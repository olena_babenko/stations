//
//  STSCalloutStationView.h
//  Stations
//
//  Created by Olena on 9/18/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "STSCalloutStationTopView.h"
#import "STSCalloutDelegate.h"

@interface STSCalloutStationView : MKAnnotationView
{
    STSCalloutStationTopView  *sub_view;
}

@property (assign, nonatomic) id<STSCalloutDelegate> delegate;
@end
