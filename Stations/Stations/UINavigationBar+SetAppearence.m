//
//  UINavigationBar+SetAppearence.m
//  Stations
//
//  Created by Olena on 9/18/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "UINavigationBar+SetAppearence.h"

@implementation UINavigationBar (SetAppearence)

+(void)customizeNavigationBar
{
    
    [self setNavigationBarStandartHeader];
    
    [self setNavigationBarBackButton];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor whiteColor],
                                UITextAttributeTextColor,
                                [UIColor clearColor],
                                UITextAttributeTextShadowColor, nil];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes: attributes
                                                forState: UIControlStateNormal];
    
}
+(void)setNavigationBarBackButton
{
    
    
    
}
+(void)setNavigationBarStandartHeader
{
    UIImage *navBarImg = [UIImage imageNamed:@"topbar"];
    [[UINavigationBar appearance] setBackgroundImage:navBarImg forBarMetrics:UIBarMetricsDefault];
    
    //it not a font that fit, but it can be easily changed
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                UITextAttributeTextColor: [UIColor whiteColor],
                          UITextAttributeTextShadowColor: [UIColor clearColor],
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, -1)],
                        UITextAttributeFont: [UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:21]
     }];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
}
@end
