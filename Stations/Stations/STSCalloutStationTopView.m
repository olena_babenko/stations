//
//  STSCalloutStationTopView.m
//  Stations
//
//  Created by Olena on 9/18/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSCalloutStationTopView.h"

@implementation STSCalloutStationTopView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)setData:(STS_Station *)data
{
    _data = data;
    _top_title.text = data.title;
    _bottom_title.text = data.subtitle;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
