//
//  UITabBarController+CustomAppearence.h
//  Stations
//
//  Created by Olena on 9/18/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarController (CustomAppearence)

-(void)customizeTabView;
@end
