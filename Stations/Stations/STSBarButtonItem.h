//
//  STSBarButtonItem.h
//  Stations
//
//  Created by Olena on 9/21/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>

// this class for pins view controller only

@interface STSBarButtonItem : UIBarButtonItem

@property(assign, nonatomic) BOOL showing_map;
@end
