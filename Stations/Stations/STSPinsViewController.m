//
//  STSPinsViewController.m
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSPinsViewController.h"
#import "STSParserCSV.h"

#import "STSSimpleStationFactory.h"
#import "STSAdvancedStationFactory.h"

// now it's only his location info
#import "STSUserInfo.h"


@interface STSPinsViewController ()

@end

@implementation STSPinsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.tabBarItem setFinishedSelectedImage:[UIImage imageNamed: @"tab_icon_pin_active"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_icon_pin"]];
        self.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Stationer";
    self.tabBarItem.title = @"";
    
    [self addNavigationBarButton];
    
    [self getData];
    
    [self addLocationChagesObserver];
}


#pragma mark - init Data functions


-(void)getData
{
    if (!data_loader) {
        data_loader = [[STS_ServerData alloc] initWithParser:[self getParser]];
    }
    
    // make weak self to prevent this object retain in block
    __weak STSPinsViewController *weakSelf =self;
    
    [data_loader getAllPinsSuccess:^(NSArray *results)
    {
        [weakSelf useData:results];
        
    } orFail:^(NSError *fail_reason)
    {
        // show alert if connection or parser failed
        
    }];
}

-(void)useData:(NSArray *)results
{
    
    
    // create new filter with every new data 
    
    if (data_filter) {
        data_filter = nil;
    }
    data_filter = [[STSFilter alloc] initWithArray:results];
    
    [self clearOldData];
    
    [self setNewData];
    
}

-(void)clearOldData
{
    [data_filter reset];
    [self.map_view removeAnnotations:self.map_view.annotations];
}

-(void)setNewData
{
    filtered_data = [data_filter getResult];
    for (id<MKAnnotation> pin in filtered_data) {
        [_map_view addAnnotation:pin];
    }
    [self.station_list reloadData];
}

// choose a parser for Data loading object

-(id<STSParser>)getParser
{
   return [[STSParserCSV alloc] init];
}

-(id<STSStationViewsFactory>)simple_factory
{
    if (!_simple_factory) {
        _simple_factory = [[STSSimpleStationFactory alloc] init];
    }
    return _simple_factory;
}
-(id<STSStationViewsFactory>)advanced_factory
{
    if (!_advanced_factory) {
        _advanced_factory = [[STSAdvancedStationFactory alloc] init];
    }
    return _advanced_factory;
}
-(id<STSStationViewsFactory>)current_factoryForObj:(STS_Station *)data_obj
{
    id<STSStationViewsFactory> current_factory= self.simple_factory;
    // change a factory depends on object property
    if (data_obj.is_advanced) {
        current_factory = self.advanced_factory;
    }
    return current_factory;
}

-(void)toDetailView:(STS_Station *)data
{
    NSLog(@"To Detail View! %@", [data title]);
}

-(void)addLocationChagesObserver
{
    STSUserInfo *user_info = [STSUserInfo sharedInstance];
    [user_info addObserver:self forKeyPath:@"current_location" options:NSKeyValueObservingOptionNew context:nil];
}


#pragma mark - Map View Delegate


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    
    STS_Station<MKAnnotation> *_annotation = view.annotation;
    if (!_annotation) {
        return;
    }
    // copy annotation to make callout view
    
    if (![_annotation is_copy]) 
    {
        // remove previouse selected annotation
        if (selected_annotation) {
            [mapView removeAnnotation:selected_annotation];
            selected_annotation = nil;
        }
        selected_annotation = [_annotation copy];
        [mapView addAnnotation:selected_annotation];
        [mapView setCenterCoordinate:selected_annotation.coordinate animated:YES];
    }
    
	 // selection of callout annotation made in its own class
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    
    STS_Station<MKAnnotation> *_annotation = view.annotation;
    
    // if there is callout view
    if (selected_annotation){
        if(!_annotation.is_copy) {
        
            [mapView removeAnnotation:selected_annotation];
            selected_annotation= nil;
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
   
    STS_Station<MKAnnotation> *_annotation = annotation;
    id<STSStationViewsFactory> current_factory= [self current_factoryForObj:_annotation];
    
    if (_annotation.is_copy) {
        return [current_factory getCalloutViewInMap:mapView withAnnotation:annotation andDelegate:self];
    }
    return [current_factory getPinViewInMap:mapView withAnnotation:annotation];
}


#pragma mark - memory warning


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - User Location Data


- (IBAction)to_current_location:(id)sender {
    
    [[STSUserInfo sharedInstance] updateLocation];
}


-(void)updateMapWithCurrentLocations
{

    [self.map_view setRegion:[[STSUserInfo sharedInstance] locationDefaultRegion]];
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"current_location"]) {
        [self updateMapWithCurrentLocations];
    }
}
-(void)dealloc
{
    @try {
        
        STSUserInfo *user_info = [STSUserInfo sharedInstance];
        [user_info removeObserver:self forKeyPath:@"current_location"];
       
    }
    @catch (NSException *exception) {
        
    }
    
}


#pragma mark - Callout Delegate


-(void)stsCalloutSelected:(id<MKAnnotation>)annotation
{
    [self toDetailView:annotation];
}


#pragma mark - To search Zip


- (IBAction)to_filterZip:(id)sender {
    
    [self searchZip:self.search_txt.text];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [self searchZip:textField.text];
    
    return YES;
}

-(void)searchZip:(NSString *)search_txt
{
   
    [self.search_txt resignFirstResponder];
    [self clearOldData];
    
    if (search_txt.length > 0) {
        
        [data_filter filterZip:search_txt];
    }
    
    [self setNewData];
    
    
}


#pragma mark - Switch Map/List Views


-(void)addNavigationBarButton
{
    top_btn = [[STSBarButtonItem alloc] initWithImage:nil style:UIBarButtonItemStylePlain target:self action:@selector(switchView)];
    self.navigationItem.rightBarButtonItem = top_btn;
    [self showMap:top_btn.showing_map];
}
-(void)switchView
{
    top_btn.showing_map = !top_btn.showing_map;
    [self showMap:top_btn.showing_map];
}
-(void)showMap:(BOOL) showing_map
{
    self.map_view.hidden = !showing_map;
    self.station_list.hidden = showing_map;
}


#pragma mark - Table View 


-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filtered_data.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    STS_Station *data_obj = filtered_data[indexPath.row];
    
    id<STSStationViewsFactory> current_factory= [self current_factoryForObj:data_obj];
    return [current_factory cellHeight];
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    STS_Station *data_obj = filtered_data[indexPath.row];
    id<STSStationViewsFactory> current_factory=[self current_factoryForObj:data_obj];
    UITableViewCell *cell = [current_factory getCellInTable:tableView withData:data_obj];
    return cell;
}

@end
