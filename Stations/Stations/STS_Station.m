//
//  STS_Station.m
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STS_Station.h"

@implementation STS_Station

-(id)initWithString:(NSString *)data_row
{
    self = [super init];
    if (self) {
        
        [self parseRow:data_row];
        
    }
    return self;
}

-(void)parseRow:(NSString *)row
{
    
    
    
    NSArray *components = [row componentsSeparatedByString:@","];
    string_for_copy = row;
    if (components.count >= [self components_number]) {

        _longitude = [self doublefromId:components[0]];
        _latitude = [self doublefromId:components[1]];
        _address = components[2];
        _zip = components[3];
        _city = components[4];
        _type = components[5];
        _is_advanced = [self boolFromString:components[6]];
        
        _is_diesel = [self boolFromString:components[7]];
        
    }
    
}
-(double)doublefromId:(NSString *)obj
{
    NSScanner *scanner = [NSScanner scannerWithString:obj];
    double d;
    BOOL success = [scanner scanDouble:&d];
    if (success)
    return d;
    
    return 0;
}

-(BOOL)boolFromString:(NSString *)data
{
    
    if ([data isEqualToString:@"\"J\""]) {
       
        return YES;
    }
    return NO;
}
-(int)components_number
{
    return 8;
}
-(NSString *)title
{
    return [NSString stringWithFormat:@"%@, %@", _type, _city];
}
-(NSString *)subtitle
{
    return [NSString stringWithFormat:@"%@", _address];
}
- (CLLocationCoordinate2D)coordinate {
	CLLocationCoordinate2D coordinate;
	coordinate.latitude = _latitude;
	coordinate.longitude = _longitude;
	return coordinate;
}
-(id)copy
{
    STS_Station *copy_obj = [[STS_Station alloc] initWithString:string_for_copy];
    copy_obj.is_copy = YES;
    return copy_obj;
}
@end
