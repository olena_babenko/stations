//
//  STSUserInfo.h
//  Stations
//
//  Created by iOS Developer on 20.09.13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

// this class can be usefull not only for location info
@interface STSUserInfo : NSObject<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    
    CLLocation *last_location;
    
}
@property (strong, nonatomic) CLLocation *current_location;

-( MKCoordinateRegion)locationDefaultRegion;

-(void)updateLocation;

+ (id)sharedInstance;
@end
