//
//  STSAdvancedStationFactory.m
//  Stations
//
//  Created by Olena on 9/17/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import "STSAdvancedStationFactory.h"
#import "STSAdvancedStationCell.h"

@implementation STSAdvancedStationFactory

-(NSString *)simplePinImageName
{
    return @"pin_truck-blue";
}
-(NSString *)simplePinName
{
    return @"AdvancedStationAnnotation";
}
-(NSString *)calloutPinName
{
    return [super calloutPinName];
}
-(MKAnnotationView *)getPinViewInMap:(MKMapView *)map withAnnotation:(id<MKAnnotation>)annotation
{
    
    return [super getPinViewInMap:map
                   withAnnotation:annotation];
    
}
-(MKAnnotationView *)getCalloutViewInMap:(MKMapView *)map withAnnotation:(id<MKAnnotation>)annotation andDelegate:(id<STSCalloutDelegate>)delegate
{
    
    // in this version callout is the same, for simple and advanced
    return [super getCalloutViewInMap:map withAnnotation:annotation andDelegate:delegate];

}
-(UITableViewCell *)getCellInTable:(UITableView *)table withData:(id)cell_data
{
    
    
    NSString *reusable_name = @"STSAdvancedStationCell";
    
    STSSimpleStationCell *cell = [table dequeueReusableCellWithIdentifier:reusable_name];
    
    if(!cell)
    {
        UINib *cell_loader = [UINib nibWithNibName:@"STSAdvancedStationCell" bundle:nil];
        NSArray *topLevelItems = [cell_loader instantiateWithOwner:table options:nil];
        cell = topLevelItems[0];
        
    }
    cell.data = cell_data;
    
    return cell;
    
}
-(CGFloat)cellHeight
{
    return 70;
}
@end
