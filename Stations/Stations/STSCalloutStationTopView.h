//
//  STSCalloutStationTopView.h
//  Stations
//
//  Created by Olena on 9/18/13.
//  Copyright (c) 2013 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STS_Station.h"

@interface STSCalloutStationTopView : UIView


@property (weak, nonatomic) IBOutlet UILabel *top_title;
@property (weak, nonatomic) IBOutlet UILabel *bottom_title;
@property (weak, nonatomic) STS_Station *data;
@end
